<?php
// first sample REST API using PHP
// list all film sell by this shop
$conn = new mysqli('localhost', 'root', '', 'sakila');
$sql = "SELECT * FROM film";
$result = $conn->query($sql);
$bil_rekod = $result->num_rows;
$arr = []; // create empty array
//$arr['total'] = $bil_rekod;
while($row = $result->fetch_assoc()) {
	$arr[] = $row;
}

$arr2 = ['total' => $bil_rekod, 'result' => $arr];

// tell client data is JSON format
header('Content-Type: application/json');

// return data dlm format JSON
echo json_encode($arr2);