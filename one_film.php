<?php
// http://localhost/training/one_film.php?film_id=10
// return one film based on "film id"
// sample of REST API with input
$conn = new mysqli('localhost', 'root', '', 'sakila');
$film_id = $_GET['film_id'];
// patut buat data validation disini
$sql = "SELECT * FROM film WHERE film_id = $film_id";
$result = $conn->query($sql);

// tell client data is JSON format. sebelum any acho
header('Content-Type: application/json');

if ($result->num_rows > 0) {
    // fetch_assoc() will reaturn an object
    echo json_encode($result->fetch_assoc());
} else {
    // id film x wujud
    echo json_encode(["status" => 'failed!']);
}