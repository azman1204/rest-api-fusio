<?php
// strict kan method submission data
$method = $_SERVER['REQUEST_METHOD']; // post/get/put/delete/patch
if ($method !== 'DELETE') {
    echo json_encode(['status' => 'wrong method']);
    return;
}

// REST API - delete film using DELETE method
$conn = new mysqli('localhost', 'root', '', 'sakila');
$film_id = $_GET['film_id'];
$sql = "DELETE FROM film WHERE film_id = $film_id";
$result = $conn->query($sql);

// tell client data is JSON format
header('Content-Type: application/json');

// return data dlm format JSON
if($result) {
    // berjaya delete
    echo json_encode(['status' => 'Success']);
} else {
    // tak berjaya delete
    echo json_encode(['status' => 'Failed']);
}
