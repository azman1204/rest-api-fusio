<?php
// sample REST API, using POST method to insert data
$conn = new mysqli('localhost', 'root', '', 'sakila');
// patut buat data validation disini
//$title = $_POST['title']; // tak valid jika client submit data guna JSON format
// convert form data json ke array / obj
$data = json_decode(file_get_contents("php://input"));
$title = $data->title;
$sql = "INSERT INTO film(title, language_id) VALUES('$title', 1)";
//echo $sql;
$result = $conn->query($sql);

header("Content-Type:application/json");

if ($result) {
    echo json_encode(['status' => 'success']);
} else {
    // tidak berjaya insert
    echo json_encode(['status' => 'failed']);
}

$conn->close();