<?php
// convert array dan object ke JSON

// normal array
$arr = ['azman', 'abu', 'ali'];
header("Content-Type:application/json");
echo json_encode($arr);

// associative array
$arr2 = ['nama' => 'azman', 'alamat' => 'puchong'];
echo json_encode($arr2);

// object
$person = new Person();
$person->name = 'John Doe';
$person->age = 40;

echo json_encode($person);

class Person {
    var $name;
    var $age;
}