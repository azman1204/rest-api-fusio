<?php
// sample REST API, using PUT/PATCH method to UPDATE data
$conn = new mysqli('localhost', 'root', '', 'sakila');
// read JSON data / form
$data = json_decode(file_get_contents("php://input"));
$title = $data->title;
$film_id = $data->film_id;
$sql = "UPDATE film SET title = '$title' WHERE film_id = $film_id";
$result = $conn->query($sql);
header("Content-Type:application/json");

if ($result) {
    // berjaya update
    echo json_encode(['status' => 'success']);
} else {
    // tidak berjaya update
    echo json_encode(['status' => 'failed']);
}

$conn->close();